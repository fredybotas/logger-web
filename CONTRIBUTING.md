# Contributing to Behametrics - Web Logger

Thank you for taking your time to contribute to Behametrics - Web Logger.
To help improve the project, we would like you to follow guidelines specified in this document.

## Submitting Changes

1. Make sure your proposed change (new feature, bug fix) is not already implemented (by searching the documentation, closed issues or merge requests).
2. Fork the repository.
3. Create a new, appropriately named branch:

       git checkout -b log-sensor-accuracy master

4. Commit your changes to the new branch.
5. Push your changes to the remote repository:

       git push origin log-sensor-accuracy

6. Create a new merge request on GitLab with `master` as the target branch.

## Environment

* Based on Webpack 4.
* ES6 as a source.
* Exports in a [umd](https://github.com/umdjs/umd) format so the library works everywhere.
* Testing with [Mocha](http://mochajs.org/) and [Chai](http://chaijs.com/).
* Linting with [ESLint](http://eslint.org/).

## Getting started

Run `npm install` from the terminal to install the project's dependencies.

## Scripts

* `npm run build` - builds a production version of the library (minified) under the `lib` directory.
* `npm run dev` - builds a development version of the library (not minified) and runs a watcher so the project will compile on file change.
* `npm run test` - runs tests.
* `npm run test:watch` - runs tests in watch mode.
* `npm run demo` - runs a demo page to demonstrate the logger's features.
  The demo assumes you are running a server (such as the [Behametrics server](https://gitlab.com/behametrics/server))
  on the localhost using the default port.
* `npm run demo-https` - runs a demo page in HTTPS to demonstrate the logger's features.
  `key.pem` and `cert.pem` files are expected to be present in the root directory.
* `npm run doc` - generates JSDoc API documentation.

## Coding and JSDoc Conventions

Follow the conventions below:
* [JavaScript Google Style Guide](https://google.github.io/styleguide/jsguide.html),
* ESLint rules along with the [project configuration](https://gitlab.com/behametrics/logger-web/blob/master/.eslintrc.json).

Conventions later in the order have precedence (i.e. ESLint rules have precedence over the Google Style Guide).

### Tests

Any new feature or bug fix should be verified by tests.
Prefer unit tests.
