import chai from 'chai';
import sinon from 'sinon';
import {Limiter} from '../../src/utils/limiter';

const expect = chai.expect;
let limiter;
const data = [];
let clock;
const chunkSize = 5;
const intervalMilliseconds = 100;

describe(`Given an instance of Limiter with chunk size of ${chunkSize} and ${intervalMilliseconds}ms interval`, () => {
  before(() => {
    clock = sinon.useFakeTimers();
    limiter = new Limiter((log) => data.push(log), chunkSize, intervalMilliseconds);
  });

  after(() => {
    limiter.terminate();
  });

  describe('When a Limiter instance is created', () => {
    it('the instance should be defined', () => {
      expect(limiter).not.to.be.undefined;
    });
  });

  const testData = [
    {
      afterInterval: [1999, 3999, 5999, 7999, 9999],
      numEvents: 10000,
      numEventsToBeBuffered: 5,
      intervalMilliseconds: 100,
      order: 'first',
    },
    {
      afterInterval: [1999, 3999, 5999, 7999, 9999, 19, 39, 59, 79, 99],
      numEvents: 100,
      numEventsToBeBuffered: 5,
      intervalMilliseconds: 200,
      order: 'second',
    },
    {
      afterInterval: [1999, 3999, 5999, 7999, 9999, 19, 39, 59, 79, 99, 0, 1, 2, 3],
      numEvents: 4,
      numEventsToBeBuffered: 4,
      intervalMilliseconds: 300,
      order: 'third',
    },
  ];

  testData.forEach((item) => {
    describe(`When ${item.numEvents} events are pushed to the limiter`
             + ` in the ${item.order} ${item.intervalMilliseconds}ms period`, () => {
      it(`${item.numEventsToBeBuffered} events should be buffered`
         + ` after the ${item.order} ${item.intervalMilliseconds}ms period`, (done) => {
        [...Array(item.numEvents).keys()].forEach((val) => limiter.pushData(val));

        clock.tick(item.intervalMilliseconds);
        expect(data).to.deep.equal(item.afterInterval);

        done();
      });
    });
  });
});
