export const testStartAndStopData = [
  {
    deviceType: 'desktop',
    inputs: ['cursor', 'wheel', 'touch'],
    expectedRunningInputs: ['cursor', 'wheel'],
  },
  {
    deviceType: 'desktop',
    inputs: ['cursor', 'touch'],
    expectedRunningInputs: ['cursor'],
  },
  {
    deviceType: 'desktop',
    inputs: ['touch'],
    expectedRunningInputs: [],
  },
  {
    deviceType: 'mobile',
    inputs: ['cursor', 'wheel', 'touch'],
    expectedRunningInputs: ['touch'],
  },
  {
    deviceType: 'mobile',
    inputs: ['cursor', 'wheel'],
    expectedRunningInputs: [],
  },
  {
    deviceType: 'desktop',
    inputs: [],
    expectedRunningInputs: [],
  },
  {
    deviceType: 'mobile',
    inputs: [],
    expectedRunningInputs: [],
  },
  {
    deviceType: 'desktop',
    inputs: ['custom'],
    expectedRunningInputs: ['custom'],
  },
  {
    deviceType: 'mobile',
    inputs: ['custom'],
    expectedRunningInputs: ['custom'],
  },
  {
    deviceType: 'unknown',
    inputs: ['cursor', 'wheel', 'touch'],
    expectedRunningInputs: [],
  },
  {
    deviceType: 'desktop',
    inputs: ['cursor', 'wheel', 'touch', 'unknown'],
    expectedRunningInputs: ['cursor', 'wheel'],
  },
];
