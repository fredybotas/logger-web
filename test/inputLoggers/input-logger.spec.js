import chai from 'chai';
import sinon from 'sinon';
import {getDefaultConfig} from '../../src/config';
import {InputLogger} from '../../src/inputLoggers/input-logger';
import {testData} from './input-logger-test-suite';

const expect = chai.expect;
let logger;
let spyLoggerStartLogging;
let spyLoggerStopLogging;

describe('Given an instance of InputLogger', () => {
  beforeEach(() => {
    logger = new InputLogger(getDefaultConfig());
    spyLoggerStartLogging = sinon.spy(logger, 'startLogging');
    spyLoggerStopLogging = sinon.spy(logger, 'stopLogging');
  });

  afterEach(() => {
    spyLoggerStartLogging.restore();
    spyLoggerStopLogging.restore();
  });

  testData.forEach((item) => {
    describe(`When the logger is started ${item.numStartCalls} times and stopped ${item.numStopCalls} times`, () => {
      it(`start() should be called ${item.expectedNumStartLoggingCalls} times`
         + ` and stop() should be called ${item.expectedNumStopLoggingCalls} times`, () => {
        for (let i = 0; i < item.numStartCalls; i++) {
          logger.start();
        }

        for (let i = 0; i < item.numStopCalls; i++) {
          logger.stop();
        }

        expect(spyLoggerStartLogging.callCount).to.equal(item.expectedNumStartLoggingCalls);
        expect(spyLoggerStopLogging.callCount).to.equal(item.expectedNumStopLoggingCalls);
        expect(logger.isRunning).to.equal(item.expectedIsRunning);
      });
    });
  });
});
