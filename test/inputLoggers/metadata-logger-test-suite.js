export const testEvents = [
  {
    expectedContent: {
      app_code_name: 'Mozilla',
      app_name: 'Netscape',
      app_version: '4.0',
      cookie_enabled: true,
      hardware_concurrency: 4,
      language: 'en-US',
      on_line: true,
      platform: '',
      product: 'Gecko',
      product_sub: '20030107',
      user_agent: 'Mozilla/5.0 (win32) AppleWebKit/537.36 (KHTML, like Gecko) jsdom/11.11.0',
      vendor: 'Apple Computer, Inc.',
      vendor_sub: '',
      plugins: [],
      logger_platform: 'web',
      screen_width: 24,
      screen_height: 24,
      screen_color_depth: 24,
    },
  },
];
