import {WHEEL_UNKNOWN_DELTA_UNIT_PREFIX} from '../../src/inputLoggers/wheel-logger';

export const testEvents = [
  {
    event: {
      buttons: 0,
    },
    expectedEventsContent: [
      {
        button: '',
      },
    ],
  },
  {
    event: {
      buttons: 2,
    },
    expectedEventsContent: [
      {
        button: 'right',
      },
    ],
  },
  {
    event: {
      buttons: 2,
      deltaMode: -1,
    },
    expectedEventsContent: [
      {
        button: 'right',
        delta_unit: `${WHEEL_UNKNOWN_DELTA_UNIT_PREFIX}-1`,
      },
    ],
  },
  {
    event: {
      buttons: 3,
    },
    expectedEventsContent: [
      {
        button: 'left',
      },
      {
        button: 'right',
      },
    ],
  },
];
