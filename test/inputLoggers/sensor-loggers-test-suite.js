import {
  AccelerometerLogger,
  GyroscopeLogger,
  LinearAccelerationLogger,
  MagneticFieldLogger,
} from '../../src/inputLoggers/sensor-loggers';


export const testEvents = [
  {
    SensorLoggerType: LinearAccelerationLogger,
    genericApiData: {
      x: 0.5,
      y: 0.1,
      z: 0.2,
    },
    deviceMotionData: {
      acceleration: {
        x: 0.5,
        y: 0.1,
        z: 0.2,
      },
    },
    expectedData: {
      input: 'sensor_linear_acceleration',
      content: {
        x: 0.5,
        y: 0.1,
        z: 0.2,
      },
    },
  },
  {
    SensorLoggerType: AccelerometerLogger,
    genericApiData: {
      x: 0.5,
      y: 0.1,
      z: 0.2,
    },
    deviceMotionData: {
      accelerationIncludingGravity: {
        x: 0.5,
        y: 0.1,
        z: 0.2,
      },
    },
    expectedData: {
      input: 'sensor_accelerometer',
      content: {
        x: 0.5,
        y: 0.1,
        z: 0.2,
      },
    },
  },
  {
    SensorLoggerType: GyroscopeLogger,
    genericApiData: {
      x: 0.5,
      y: 0.1,
      z: 0.2,
    },
    deviceMotionData: {
      rotationRate: {
        alpha: 0.5,
        beta: 0.1,
        gamma: 0.2,
      },
    },
    expectedData: {
      input: 'sensor_gyroscope',
      content: {
        x: 0.5,
        y: 0.1,
        z: 0.2,
      },
    },
  },
  {
    SensorLoggerType: MagneticFieldLogger,
    genericApiData: {
      x: 0.5,
      y: 0.1,
      z: 0.2,
    },
    expectedData: {
      input: 'sensor_magnetic_field',
      content: {
        x: 0.5,
        y: 0.1,
        z: 0.2,
      },
    },
  },
];
