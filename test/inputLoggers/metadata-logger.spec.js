import chai from 'chai';
import sinon from 'sinon';
import {getDefaultConfig} from '../../src/config';
import {MetadataLogger} from '../../src/inputLoggers/metadata-logger';
import {testEvents} from './metadata-logger-test-suite';
import {INPUT_METADATA} from '../../src/inputLoggers/input-names';

const expect = chai.expect;
let logger;

describe('Given an instance of MetadataLogger', () => {
  before(() => {
    logger = new MetadataLogger(getDefaultConfig());
  });

  beforeEach(() => {
    sinon.stub(window.performance, 'now').returns(456);
  });

  afterEach(() => {
    window.performance.now.restore();
  });

  describe('When a MetadataLogger instance is created', () => {
    it('the instance should be defined', () => {
      expect(logger).not.to.be.undefined;
    });
  });

  testEvents.forEach((eventData) => {
    describe(`When a metadata event is obtained`, () => {
      it(`the event should contain device metadata in expected format`, () => {
        const expectedResult = {
          input: INPUT_METADATA,
          timestamp: 456,
          content: eventData.expectedContent,
        };

        const result = logger.getMetadata();

        expect(result).to.have.keys(Object.keys(expectedResult));
        expect(result.input).to.equal(INPUT_METADATA);
        expect(result.content).to.have.keys(Object.keys(expectedResult.content));
        expect(result.content.logger_platform).to.equal('web');
      });
    });
  });
});

describe('Given an instance of MetadataLogger', () => {
  beforeEach(() => {
    logger = new MetadataLogger(getDefaultConfig(), sinon.spy());
    logger.getMetadata = sinon.spy();
  });

  describe(`When log() is called`, () => {
    it(`the function returns a metadata event if the logger is running`, () => {
      logger.log();

      expect(logger.getMetadata.notCalled).to.be.true;
    });

    it(`the function does nothing if the logger is not running`, () => {
      logger.start();
      logger.log();

      expect(logger.getMetadata.calledOnce).to.be.true;
    });
  });
});
