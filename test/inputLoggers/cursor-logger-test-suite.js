export const testEvents = [
  {
    event: {
      type: 'mousedown',
      button: 0,
    },
    expectedEventsContent: [
      {
        event_type: 'down',
        button: 'left',
      },
    ],
  },
  {
    event: {
      type: 'mousedown',
      button: 1,
    },
    expectedEventsContent: [
      {
        event_type: 'down',
        button: 'middle',
      },
    ],
  },
  {
    event: {
      type: 'mousedown',
      button: 2,
    },
    expectedEventsContent: [
      {
        event_type: 'down',
        button: 'right',
      },
    ],
  },
  {
    event: {
      type: 'mouseup',
      button: 1,
    },
    expectedEventsContent: [
      {
        event_type: 'up',
        button: 'middle',
      },
    ],
  },
  {
    event: {
      type: 'mousedown',
      button: -1,
    },
    expectedEventsContent: [
      {
        event_type: 'down',
        button: 'button_-1',
      },
    ],
  },
  {
    event: {
      type: 'invalid',
      buttons: 0b1100000,
    },
    expectedEventsContent: [
      {
        event_type: 'other_invalid',
        button: `buttons_${0b1100000}`,
      },
    ],
  },
  {
    event: {
      type: 'mousemove',
      buttons: 0,
    },
    expectedEventsContent: [
      {
        event_type: 'move',
        button: '',
      },
    ],
  },
  {
    event: {
      type: 'mousemove',
      buttons: 1,
    },
    expectedEventsContent: [
      {
        event_type: 'move',
        button: 'left',
      },
    ],
  },
  {
    event: {
      type: 'mousemove',
      buttons: 2,
    },
    expectedEventsContent: [
      {
        event_type: 'move',
        button: 'right',
      },
    ],
  },
  {
    event: {
      type: 'mousemove',
      buttons: 4,
    },
    expectedEventsContent: [
      {
        event_type: 'move',
        button: 'middle',
      },
    ],
  },
  {
    event: {
      type: 'mousemove',
      buttons: 3,
    },
    expectedEventsContent: [
      {
        event_type: 'move',
        button: 'left',
      },
      {
        event_type: 'move',
        button: 'right',
      },
    ],
  },
  {
    event: {
      type: 'mousemove',
      buttons: 7,
    },
    expectedEventsContent: [
      {
        event_type: 'move',
        button: 'left',
      },
      {
        event_type: 'move',
        button: 'right',
      },
      {
        event_type: 'move',
        button: 'middle',
      },
    ],
  },
  {
    event: {
      type: 'mousemove',
      buttons: 0b1100000,
    },
    expectedEventsContent: [
      {
        event_type: 'move',
        button: `buttons_${0b1100000}`,
      },
    ],
  },
];
