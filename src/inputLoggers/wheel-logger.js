import {getActiveButtons} from '../utils/buttons';
import {InputLogger} from './input-logger';
import {INPUT_WHEEL} from './input-names';

const WHEEL_LISTENER_TYPE_NAME = 'wheel';
const WHEEL_DELTA_UNIT_NAME_VALUE_MAP = new Map([
  [0x00, 'pixel'],
  [0x01, 'line'],
  [0x02, 'page'],
]);
export const WHEEL_UNKNOWN_DELTA_UNIT_PREFIX = 'unit_';

/**
 * Class responsible for logging events associated with rotating the wheel button on pointing devices or some other
 * action representing wheel rotation.
 */
export class WheelLogger extends InputLogger {
  /**
   * Starts logging wheel events.
   * @protected
   */
  startLogging() {
    this.handlerReference_ = this.eventHandler_.bind(this);

    document.addEventListener(WHEEL_LISTENER_TYPE_NAME, this.handlerReference_);
  }

  /**
   * Stops logging wheel events.
   * @protected
   */
  stopLogging() {
    document.removeEventListener(WHEEL_LISTENER_TYPE_NAME, this.handlerReference_);
  }

  /**
   * Returns an array of wheel events.
   * @param {WheelEvent} event Raw wheel event.
   */
  getWheelEvents(event) {
    const inputEvents = [];

    const buttons = getActiveButtons(event);

    buttons.forEach((button) => {
      inputEvents.push({
        input: INPUT_WHEEL,
        timestamp: event.timeStamp,
        content: {
          button: button,
          delta_x: event.deltaX,
          delta_y: event.deltaY,
          delta_z: event.deltaZ,
          delta_unit: WheelLogger.getDeltaUnit(event),
          cursor_x: event.screenX,
          cursor_y: event.screenY,
        },
      });
    });

    return inputEvents;
  }

  /**
   * Returns the unit in which the wheel rotation amount is reported.
   * @param {WheelEvent} event Raw wheel event.
   */
  static getDeltaUnit(event) {
    const deltaUnit = WHEEL_DELTA_UNIT_NAME_VALUE_MAP.get(event.deltaMode);
    return deltaUnit != null ? deltaUnit : `${WHEEL_UNKNOWN_DELTA_UNIT_PREFIX}${event.deltaMode}`;
  }

  /**
   * Handles wheel events.
   * @private
   * @param {WheelEvent} event Raw wheel event.
   */
  eventHandler_(event) {
    this.logEventsFunction_(this.getWheelEvents(event));
  }
}
