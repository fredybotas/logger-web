import {getActiveButtons, getButtonPressed} from '../utils/buttons';
import {InputLogger} from './input-logger';
import {INPUT_CURSOR} from './input-names';

const CURSOR_EVENT_TYPES = new Map([
  ['mousedown', 'down'],
  ['mouseup', 'up'],
  ['mousemove', 'move'],
]);
const CURSOR_UNKNOWN_EVENT_TYPE_PREFIX = 'other_';

/**
 * Class responsible for logging events from pointing devices controlling the cursor (such as a mouse or a touchpad).
 */
export class CursorLogger extends InputLogger {
  /**
   * Starts logging cursor events.
   * @protected
   */
  startLogging() {
    this.handlerReference_ = this.eventHandler_.bind(this);

    CURSOR_EVENT_TYPES.forEach((eventType, rawEventType) => {
      document.addEventListener(rawEventType, this.handlerReference_);
    });
  }

  /**
   * Stops logging cursor events.
   * @protected
   */
  stopLogging() {
    CURSOR_EVENT_TYPES.forEach((eventType, rawEventType) => {
      document.removeEventListener(rawEventType, this.handlerReference_);
    });
  }

  /**
   * Returns an array of cursor events.
   * @param {MouseEvent} event Raw mouse event.
   */
  getCursorEvents(event) {
    const inputEvents = [];

    const eventType = CursorLogger.getEventType(event);
    const buttons = CursorLogger.getCursorButtons(event);

    buttons.forEach((button) => {
      inputEvents.push({
        input: INPUT_CURSOR,
        timestamp: event.timeStamp,
        content: {
          event_type: eventType,
          button: button,
          x: event.screenX,
          y: event.screenY,
        },
      });
    });

    return inputEvents;
  }

  /**
   * Returns the cursor event type.
   * @param {MouseEvent} event Raw mouse event.
   */
  static getEventType(event) {
    const eventType = CURSOR_EVENT_TYPES.get(event.type);
    return eventType != null ? eventType : `${CURSOR_UNKNOWN_EVENT_TYPE_PREFIX}${event.type}`;
  }

  /**
   * Returns an array of button names depending on the value of `event.type`.
   * @param {MouseEvent} event Raw mouse event.
   */
  static getCursorButtons(event) {
    if (event.type === 'mouseup' || event.type === 'mousedown') {
      return [getButtonPressed(event)];
    } else {
      return getActiveButtons(event);
    }
  }

  /**
   * Handles cursor events.
   * @private
   * @param {MouseEvent} event Raw mouse event.
   */
  eventHandler_(event) {
    this.logEventsFunction_(this.getCursorEvents(event));
  }
}
