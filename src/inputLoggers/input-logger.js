/**
 * Class providing logging input events.
 * This class does not perform any logging, use one of the appropriate subclasses to log events from a specific input.
 */
export class InputLogger {
  /**
   * @param {!Object} config Logger configuration.
   * @param {function(!Array): undefined} logEventsFunction Function handling logging input events.
   */
  constructor(config, logEventsFunction) {
    this.config_ = config;
    this.logEventsFunction_ = logEventsFunction;

    /** True if the logger is running ({@link start} was called), false otherwise. */
    this.isRunning = false;
  }

  /**
   * Starts logging input events. Subsequent calls to this method without first calling {@link stop} will have no
   * effect.
   */
  start() {
    if (!this.isRunning) {
      this.isRunning = true;
      this.startLogging();
    }
  }

  /**
   * Stops logging input events. Calling this method before calling {@link start} has no effect.
   */
  stop() {
    if (this.isRunning) {
      this.stopLogging();
      this.isRunning = false;
    }
  }

  /**
   * Starts logging input events. This method should be overridden in subclasses.
   * @protected
   */
  startLogging() {
  }

  /**
   * Stops logging input events. This method should be overridden in subclasses.
   * @protected
   */
  stopLogging() {
  }
}
