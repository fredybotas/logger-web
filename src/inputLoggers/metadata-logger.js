import snake from 'to-snake-case';
import {InputLogger} from './input-logger';
import {isObject} from '../utils/utils';
import {INPUT_METADATA} from './input-names';

/**
 * Class responsible for logging device metadata such as browser name, version or screen properties (width, height).
 */
export class MetadataLogger extends InputLogger {
  /**
   * Logs an event containing metadata associated with the current device.
   */
  log() {
    if (this.isRunning) {
      this.logEventsFunction_([this.getMetadata()]);
    }
  }

  /**
   * Returns an object representing device metadata.
   */
  getMetadata() {
    const navigatorProperties = {};
    const plugins = [];

    // Get all non-object properties from the navigator object.
    for (const property in navigator) {
      if (property && !isObject(navigator[property])) {
        navigatorProperties[snake(property)] = navigator[property];
      }
    }

    // Get list of installed plugins.
    for (const property in navigator.plugins) {
      if (property && navigator.plugins[property] instanceof window.Plugin) {
        plugins.push(navigator.plugins[property].name);
      }
    }

    const content = {
      ...navigatorProperties,
      plugins,
      logger_platform: 'web',
      screen_width: screen.width,
      screen_height: screen.height,
      screen_color_depth: screen.colorDepth,
    };

    return {
      input: INPUT_METADATA,
      timestamp: window.performance.now(),
      content: content,
    };
  }
}
