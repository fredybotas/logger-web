import {InputLogger} from './input-logger';
import {INPUT_CUSTOM_EVENT} from './input-names';

/**
 * Class responsible for logging custom events.
 */
export class CustomEventLogger extends InputLogger {
  /**
   * Logs a custom event.
   * @param {!Object} content Contents of the custom event.
   * @param {string} inputName Input name.
   */
  log(content, inputName = INPUT_CUSTOM_EVENT) {
    if (this.isRunning) {
      this.logEventsFunction_([this.getCustomEvent(content, inputName)]);
    }
  }

  /**
   * Returns a custom event.
   * @param {!Object} content Contents of the custom event.
   * @param {string} inputName Input name.
   */
  getCustomEvent(content, inputName) {
    return {
      input: inputName,
      timestamp: window.performance.now(),
      content: {...content},
    };
  }
}
