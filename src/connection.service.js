/**
 * Sends POST data to the specified backend in the JSON format.
 * @param {*} data Data to be sent.
 * @param {?string} url Server URL.
 * @param {string} endpointUrl Endpoint URL relative to `url`.
 * @param {string} loggerHeader Value for the `'Logger'` header in POST requests indicating the type of logger.
 */
export async function postData(data, url, endpointUrl, loggerHeader) {
  try {
    await fetch(`${url}/${endpointUrl}`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        'Logger': loggerHeader,
      },
    });
  } catch (error) {
    return console.error('Error sending logged events:', error);
  }
}

/**
 * Returns a session ID from the specified backend in the JSON format.
 * @param {*} data Data to be sent.
 * @param {?string} url Server URL.
 * @param {string} endpointUrl Endpoint URL relative to `url`.
 * @param {string} loggerHeader Value for the `'Logger'` header in POST requests indicating the type of logger.
 */
export async function getSessionId(data, url, endpointUrl, loggerHeader) {
  try {
    const response = await fetch(`${url}/${endpointUrl}`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        'Logger': loggerHeader,
      },
    });
    return (await response.json()).id;
  } catch (error) {
    return console.error('Error:', error);
  }
}
