const PRESSED_BUTTONS = new Map([
  [0, 'left'],
  [1, 'middle'],
  [2, 'right'],
]);
const UNKNOWN_PRESSED_BUTTON_PREFIX = 'button_';

const ACTIVE_BUTTONS = new Map([
  [1, 'left'],
  [2, 'right'],
  [4, 'middle'],
  [8, 'button_4'],
  [16, 'button_5'],
]);
const UNKNOWN_ACTIVE_BUTTONS_PREFIX = 'buttons_';

/**
 * Returns an array of button names being currently pressed (from {@link MouseEvent.buttons}).
 * @param {MouseEvent} event Raw mouse event.
 */
export function getActiveButtons(event) {
  if (!event.buttons) {
    return [''];
  }

  const buttons = [];
  let remainingButtonNums = event.buttons;

  ACTIVE_BUTTONS.forEach((buttonValue, buttonNum) => {
    if ((remainingButtonNums & buttonNum) !== 0) {
      buttons.push(buttonValue);
    }

    remainingButtonNums = remainingButtonNums & ~buttonNum;
  });

  if (remainingButtonNums) {
    buttons.push(`${UNKNOWN_ACTIVE_BUTTONS_PREFIX}${remainingButtonNums}`);
  }

  return buttons;
}

/**
 * Returns the name of the button that was pressed or released (from {@link MouseEvent.button}).
 * @param {MouseEvent} event Raw mouse event.
 */
export function getButtonPressed(event) {
  const button = PRESSED_BUTTONS.get(event.button);
  return button != null ? button : `${UNKNOWN_PRESSED_BUTTON_PREFIX}${event.button}`;
}
