#!/bin/bash

set -e

# Taken from: https://stackoverflow.com/a/246128
SCRIPT_DIRPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

REPOSITORY_DIRPATH="$(dirname "$SCRIPT_DIRPATH")"
UPDATE_BRANCH='master'

if [[ -z "$1" ]] || [[ -z "$2" ]]; then
  echo "Run with arguments <version> <gitlab-token>"
  exit 1
fi

echo "Are you on a clean '$UPDATE_BRANCH' branch? y/n"
read answer
if [[ "$answer" = "n" ]]; then
  echo "Please switch to the '$UPDATE_BRANCH' branch and commit or discard any working changes"
  exit 1
fi

branch="$(git rev-parse --abbrev-ref HEAD)"
if [[ "$branch" != "$UPDATE_BRANCH" ]]; then
  echo "Wrong branch, switch to '$UPDATE_BRANCH' to proceed"
  exit 1
fi

branch_status="$(git status --porcelain)"
if [[ "$branch_status" ]]; then
  echo "Branch '$UPDATE_BRANCH' is not clean, please commit or discard any working changes"
  exit 1
fi

PROJECT_ID=8780979
APPROVALS_BEFORE_MERGE=1

npm run build

version="$(npm version --no-git-tag-version "$1")"
version="${version#v}"

title="Release $version"
release_branch="Release-${version}"

git checkout -b "$release_branch"

git add "${REPOSITORY_DIRPATH}/package.json"
git add "${REPOSITORY_DIRPATH}/package-lock.json"
git commit -m "$title"
git tag "$version"
git push origin "$release_branch" --tags
git checkout master

curl --data "source_branch=${release_branch}&target_branch=release&title=${title}&remove_source_branch=true&allow_collaboration=true&approvals_before_merge=${APPROVALS_BEFORE_MERGE}" --header "PRIVATE-TOKEN: $2" "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests"
curl --data "source_branch=${release_branch}&target_branch=master&title=${title}&remove_source_branch=true&allow_collaboration=true&approvals_before_merge=${APPROVALS_BEFORE_MERGE}" --header "PRIVATE-TOKEN: $2" "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests"
